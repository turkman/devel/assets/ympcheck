# ymp version check
This repository uses for checking version from alpine and archlinux.

## Usage:

1. clone aports from alpine:
```
git clone https://git.alpinelinux.org/aports/ --depth=1
```
2. clone turkman repository:
```
git clone https://gitlab.com/turkman/packages/main --depth=1
```
3. clone this repository:
```
git clone https://gitlab.com/turkman/devel/assets/ympcheck --depth=1
```
4. Use like this
```
export APORTS=<aports repository directory path>
bash check.sh "<turkman repository path>" | grep "out of date" | sed "s/\x1B\[[0-9;]*[JKmsu]//g" | sort | tee -a ymp-check
```
