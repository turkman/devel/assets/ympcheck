#!/bin/bash
set +e
f=$(mktemp)
red="\033[31;1m"
yellow="\033[33;1m"
reset="\033[;0m"
check_out_of_date_arch(){
    link=$(curl https://archlinux.org/packages/?q=$1 2>/dev/null| grep -v unstable | grep ">$1<" | grep "<td><a href=" | head -n1 | cut -f2 -d"\"")
    if [[ $link == "" ]] ; then
        return
    fi
    timeout 5 curl https://archlinux.org/$link > $f
    if [[ $? -ne 0 ]] ; then
        echo -e "Arch:$1 Check failed!" 
        return
    fi
    # check outdate
    ver=$(cat $f | grep "<h2>" | grep "$1" | head -n 1 \
            | cut -f2 -d">" | cut -f1 -d"<" \
            | cut -f2 -d" " | cut -f1 -d"-" | cut -f2 -d":")
    if [[ "$ver" == "" ]] ; then
        return
    elif is_ood $ver $2 ; then
        echo -e "Arch:\t${red}$1${reset}\t(${yellow}$ver - $2${reset}) out of date." 
    fi
}

check_out_of_date_alpine(){
    find $APORTS | grep "APKBUILD" | grep "/$1/" | head -n 1
    file=$(find $APORTS | grep "APKBUILD" | grep "/$1/" | head -n 1)
    if [ ! -f $file ] ;then
        echo "$1"
        exit 1
    fi
    ver=$(bash -c "set -e ; source $file ; echo \$pkgver")
    if [[ "$ver" == "" ]] ; then
        return 1
    elif is_ood $ver $2 ; then
        echo -e "Alpine:\t${red}$1${reset}\t(${yellow}$ver - $2${reset}) out of date." 
    fi

}

is_ood(){
    data=$(echo "$2\n$1" | sort -V)
    [[ "$data" != "$1\n$2" ]]
    return $?
}

get_ympbuild_ver(){
    bash -c 'set +e; source '$1' &>/dev/null ; echo $version'
}

get_name_alias(){
    if grep "^$1:" aliases >/dev/null; then
        echo $(grep "^$1:" aliases | cut -f2 -d":")
    else
        echo "$1"
    fi
}

if [[ ! -d $1  ]] ; then
    echo "Usage: check.sh <ymp-spec-repository>"
    echo "For alpine define APORTS environment"
    exit 1
fi

dir="$(realpath $1)"
find $dir/* -type f | grep ympbuild | xargs dirname | \
while read line ; do
    name=$(basename $line)
    name=$(echo $name | sed "s/py3-/python-/g")
    name=$(get_name_alias $name)
    echo "Check:" $(basename $line)
    check_out_of_date_alpine ${name,,} $(get_ympbuild_ver $line/ympbuild) || \
    check_out_of_date_arch ${name,,} $(get_ympbuild_ver $line/ympbuild)
done
rm -f $f
